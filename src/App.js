import './App.css';
import DropDown from './components/DropDown';
import DatePicker from './components/DatePicker';
import Button from './components/Button';
import Availability from './components/Availability';
import EmptyAvailability from './components/EmptyAvailaiblity';
import NotFound from './components/NotFound';
import {useEffect, useState} from 'react';
import {get} from './functions/api'
 
function App() {
  const [hotelsList, setHotelList] = useState([]);
  const [hotel, setHotel] = useState(null);
  const [checkIn, setCheckin] = useState(null);
  const [checkOut, setCheckout] = useState(null);
  const [availabilities, setAvailabilities] = useState(null);
  let today = new Date().toISOString().slice(0, 10)


  async function getAvailabilities(){
    const data = await get('/api/availability/' + hotel + '/' + checkIn + '/' + checkOut + '/');
    console.log(data)
    setAvailabilities(data)
  }

  useEffect(() => {
    const fetchData = async () => {
      const data = await get('/api/hotels/');
      setHotelList(data['hotels'])
    }

    fetchData()
  }, [])

  return (
    <div className="App">
      <div className="title-component">
        <h1 className="title">Availability</h1>
        <hr/>
      </div>
      <div className="form-component">
        <DropDown
          hotelList={hotelsList}
          setHotel={setHotel}
        />
        <DatePicker 
          setDatePicker={setCheckin}
          min={today}
          disabled={false}
        />
        <DatePicker
          setDatePicker={setCheckout}
          min={checkIn}
          disabled={!checkIn}
        />
        <Button 
          getAvailabilities={getAvailabilities}
          disabled={!hotel || !checkIn || !checkOut}
        />
      </div>
      <div>
        {
          availabilities ? (
            availabilities.length > 0 ? (
              availabilities.map((item, idx) =>{  
                return <Availability 
                  item={item}
                  key={idx}
                />
              })
            ) : (
              <NotFound/>
            )

          ): (
            <EmptyAvailability/>
          )
        }
      </div>
    </div>
  );
}

export default App;
