import React from "react";
import '../stylesheets/Availability.css'

function Availability({item}){
    return(
        <div>
            <div className="availibility-component">
                <div className="header">
                    <p style={{color:'white', fontWeight:'bold'}}>{item.code}</p>
                </div>
                {
                    item.rates.map((itemRate, idx) =>{ 
                        return (
                            <div className="body" key={idx}>
                                <p>{itemRate.code}</p>
                                <p>{itemRate.total_price}€</p>
                            </div>
                        )
                    })
                }
            </div>
        </div>    
    );
}
export default Availability;