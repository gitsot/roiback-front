import React from "react";
import '../stylesheets/Button.css'

function Button(props){
    return(
        <div className="button-component">
            <button className="button" disabled={props.disabled} onClick={props.getAvailabilities}>Click Me!</button>
        </div>
    );
}
export default Button;