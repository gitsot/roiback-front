import React from "react";
import '../stylesheets/DatePicker.css'

function DatePicker(props){
    return(
        <div className="date-picker-component">
            <input type="date" className="date-picker" min={props.min} disabled={props.disabled} onChange={(e)=> props.setDatePicker(e.target.value)}/>
        </div>
    );
}
export default DatePicker;