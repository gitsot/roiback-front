import React from "react";
import '../stylesheets/DropDown.css'


function DropDown(props){
    return(
        <div className='drop-down-component'>
            <select className="drop-down" onChange={(event) => props.setHotel(event.target.value)}>
                <option value={""}>Choose Hotel</option>
                {props.hotelList.map((item, idx) => <option value={item} key={idx}>{item}</option>)}
            </select>
        </div>
    );
}
export default DropDown;