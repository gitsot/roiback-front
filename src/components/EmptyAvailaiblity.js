import React from "react";
import '../stylesheets/EmptyAvailability.css'
import { BsSearch } from 'react-icons/bs';

function EmptyAvailability(){
    return(
        <div className="empty-availability-component">
            <div>
                <BsSearch size={80} style={{color:'#D8D8D8'}}/>
            </div>
                <p>CHECK AVAILABILITY</p>
            <p style={{color:'grey', textAlign:'center', width:'50%'}}>Select a hotel and twho dates and you will receive magical results</p>
        </div>
    );
}
export default EmptyAvailability;