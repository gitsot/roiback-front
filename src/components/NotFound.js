import React from "react";
import '../stylesheets/NotFound.css'

function NotFound({item}){
    return(
        <div className="not-found-component">
            <p>No results found</p>
        </div>
    );
}
export default NotFound;