/**
 * This function makes a GET call to the API, sending the url as a parameter.
 * @param {string} url: '/api/hotels/'
 * @returns json
 */
// TODO: manejar los errores
export async function get(url) {
    const response = await fetch(url);

    if (!response.ok) {
      const message = `An error has occured: ${response.status}`;
      throw new Error(message);
    }
    const items = await response.json();
    return items;
  }
